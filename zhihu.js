// @run-at document-start
// @type bookmarklet
(function () {
  /* 判断是否该执行 */
  const whiteList = ['zhihu.com'];
  const hostname = window.location.hostname;
  const key = encodeURIComponent('谷花泰:知乎直接看:执行判断');

  const result = whiteList.some(site => {
    if (hostname.match(site)) {
      return true;
    }
    return false;
  });

  if (!result || window[key]) {
    return;
  };

  window[key] = true;

  /* 代码正文 */
  Object.defineProperties(window.navigator, {
    'userAgent': {
      enumerable: true,
      value: 'Mozilla/5.0 (Windows Phone 10)'
    },
    'appVersion': {
      enumerable: true,
      value: '5.0 (Windows Phone 10)'
    },
    'platform': {
      enumerable: true,
      value: 'Win32'
    }
  });
})();